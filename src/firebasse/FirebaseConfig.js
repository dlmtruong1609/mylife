import * as firebase from 'firebase'


// Your web app's Firebase configuration
let firebaseConfig = {
    apiKey: "AIzaSyAkO30TAuo8W2Et-LWEMhSL8ErPfRdkv44",
    authDomain: "mylife-8be09.firebaseapp.com",
    databaseURL: "https://mylife-8be09.firebaseio.com",
    projectId: "mylife-8be09",
    storageBucket: "mylife-8be09.appspot.com",
    messagingSenderId: "1071735463771",
    appId: "1:1071735463771:web:54b68c097a592f8ea8c47e",
    measurementId: "G-4LVBM44ZLL"
  };
  
  // Initialize Firebase
  export default firebaseConfig = firebase.initializeApp(firebaseConfig);
