import React, { useState, useEffect } from 'react';

import { BrowserRouter } from 'react-router-dom';

import './App.css';

import Direction from './router/Direction';
import BannerArea from './component/home/BannerArea';
import FooterArea from './component/home/FooterArea';
import HeaderArea from './component/home/HeaderArea';
 function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <BannerArea/>
        <HeaderArea/>
        <Direction/>
        <FooterArea/>
      </div>
  </BrowserRouter>
  );
}

export default App;
