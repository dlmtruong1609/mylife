import FirebaseConfig from "../firebasse/FirebaseConfig";

let db = FirebaseConfig.firestore();
let data = db.collection('travel');
class DAO {
    add(obj) {
        data.add(obj)
        .then((docRef) => {
            console.log("Document written with ID: ", docRef.id);
        })
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
    }
    readAll() {
        data.get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${doc.data()}`);
            });
        });
    }
    async readById(id) {
        let result = {};
        await data.doc(id).get().then((doc) => {
            if (doc.exists) {
                result = doc.data();
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
         return result;
    }
    deleteById(id) {
        data.doc(id).delete().then(() => {
            console.log("Document successfully deleted!");
        }).catch((error) => {
            console.error("Error removing document: ", error);
        });
    }
    updateById(id, obj) {
        // data.doc(id).set(obj); or
        data.doc(id).update(obj)
        .then(() => {
            console.log("Document successfully updated!");
        });
    }
}
export default DAO