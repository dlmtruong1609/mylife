import React from 'react';

import 'owl.carousel/dist/assets/owl.theme.default.css';

import EditorPickArea from '../component/home/EditorPickArea';
import InternalNews from '../component/home/InternalNews';
import PopularNewsFeed from '../component/home/PopularNewsFeed';
import 'owl.carousel/dist/assets/owl.carousel.min.css'
const HomePage = () => {
    return (
        <div>
            <EditorPickArea/>
            <InternalNews/>
            <PopularNewsFeed/>
        </div>
    );
};

export default HomePage;