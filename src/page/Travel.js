import React from 'react';

import MainArea from '../component/pages/MainArea';
import TopPostArea from '../component/pages/TopPostArea';

const Travel = () => {
    return (
        <div>
            <TopPostArea/>
            <MainArea/>
        </div>
    );
};

export default Travel;