import React, { useEffect, useState } from 'react';

import TravelPostModel from '../../model/TravelPostModel';
import TravelPostServices from '../../services/TravelPostServices';

import f1 from '../../assets/img/blog/feature-img1.jpg'
import s1 from '../../assets/img/blog/s-img.jpg'
import c1 from '../../assets/img/blog/c1.jpg'
import c2 from '../../assets/img/blog/c2.jpg'
import c3 from '../../assets/img/blog/c3.jpg'
import c4 from '../../assets/img/blog/c4.jpg'
import c5 from '../../assets/img/blog/c5.jpg'
import prev from  '../../assets/img/blog/prev.jpg'
import next from  '../../assets/img/blog/next.jpg'

const PostDetails = () => {
    const [title, setTitle] = useState('');
    let readPost = async () => {
      const travelPostService = new TravelPostServices();
      const travelPostModel = await travelPostService.readById("nYorUM6UkpWqVtnh7ovB");
      
      setTitle(travelPostModel.author)
    }
    useEffect(() => {
      readPost();
    })
    return (
        <section className="post-content-area single-post-area">
        <button type="" onClick={() => {console.log(title);
        }}>OK</button>
        <div className="container">
          <div className="row">
            <div className="col-lg-9 posts-list">
              <div className="post-content single-post-blog row">
                <div className="col-lg-12">
                  <div className="feature-img">
                    <img className="img-fluid" src={f1} alt="" />
                  </div>
                </div>
                <div className="col-lg-12">
                  <h1 className="mt-20 mb-20">Cartridge Is Better Than Ever <br />
                    A Discount Toner</h1>
                  <div className="row tags">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div>
                        <a href="#" className="primary-btn mr-10">Lifestyle</a>
                        <a href="#" className="primary-btn">Gadget</a>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 text-right">
                      <div className="d-flex justify-content-end">
                        <div className="mr-15">
                          <h5 className="text-white">Mark wiens</h5>
                          <p>12 Dec, 2017 11:21 am</p>
                        </div>
                        <div>
                          <img className="img-fluid" src={s1} alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <p className="excert">
                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to
                    spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price.
                    However, who has the willpower
                    to actually sit through a self-imposed MCSE training.
                  </p>
                  <p>
                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to
                    spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price.
                    However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to
                    actually sit through a self.
                  </p>
                </div>
                <div className="col-lg-12">
                  <div className="quote-box">
                    <div className="quotes">
                      MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to
                      spend money
                      on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who
                      has the willpower to actually sit through .
                    </div>
                  </div>
                  <div className="row mb-30">
                    <div className="col-lg-12 mt-30">
                      <p>
                        MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to
                        spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price.
                        However, who has the willpower
                        to actually sit through a self-imposed MCSE training.
                      </p>
                      <p>
                        MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to
                        spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price.
                        However, who has the willpower
                        to actually sit through a self-imposed MCSE training.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tag-details">
                <div className="row">
                  <div className="col-lg-4 col-md-4">
                    <p>
                      <a href="#">
                        <span className="lnr lnr-heart" />
                        Lily and 4 people like this
                      </a>
                    </p>
                  </div>
                  <div className="col-lg-4 col-md-4">
                    <p>
                      <a href="#">
                        <span className="lnr lnr-bubble" />
                        06 Comments
                      </a>
                    </p>
                  </div>
                  <div className="col-lg-4 col-md-4 text-right">
                    <p>
                      <a href="#">
                        <span className="fa fa-facebook" />
                      </a>
                      <a href="#">
                        <span className="fa fa-twitter" />
                      </a>
                      <a href="#">
                        <span className="fa fa-dribbble" />
                      </a>
                      <a href="#">
                        <span className="fa fa-behance" />
                      </a>
                    </p>
                  </div>
                </div>
              </div>
              <div className="navigation-area">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                    <div className="thumb">
                      <a href="#"><img className="img-fluid" src={prev} alt="" /></a>
                    </div>
                    <div className="arrow">
                      <a href="#"><span className="lnr text-white lnr-arrow-left" /></a>
                    </div>
                    <div className="detials">
                      <p>Prev Post</p>
                      <a href="#">
                        <h4>Space The Final Frontier</h4>
                      </a>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                    <div className="detials">
                      <p>Next Post</p>
                      <a href="#">
                        <h4>Telescopes 101</h4>
                      </a>
                    </div>
                    <div className="arrow">
                      <a href="#"><span className="lnr text-white lnr-arrow-right" /></a>
                    </div>
                    <div className="thumb">
                      <a href="#"><img className="img-fluid" src={next} alt="" /></a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="comments-area">
                <h4>05 Comments</h4>
                <div className="comment-list">
                  <div className="single-comment justify-content-between d-flex">
                    <div className="user justify-content-between d-flex">
                      <div className="thumb">
                        <img src={c1} alt="" />
                      </div>
                      <div className="desc">
                        <h5><a href="#">Emilly Blunt</a></h5>
                        <p className="date">December 4, 2017 at 3:12 pm </p>
                        <p className="comment">
                          Never say goodbye till the end comes!
                        </p>
                      </div>
                    </div>
                    <div className="reply-btn">
                      <a className="btn-reply">reply</a>
                    </div>
                  </div>
                </div>
                <div className="comment-list left-padding">
                  <div className="single-comment justify-content-between d-flex">
                    <div className="user justify-content-between d-flex">
                      <div className="thumb">
                        <img src={c2} alt="" />
                      </div>
                      <div className="desc">
                        <h5><a href="#">Elsie Cunningham</a></h5>
                        <p className="date">December 4, 2017 at 3:12 pm </p>
                        <p className="comment">
                          Never say goodbye till the end comes!
                        </p>
                      </div>
                    </div>
                    <div className="reply-btn">
                      <a className="btn-reply">reply</a>
                    </div>
                  </div>
                </div>
                <div className="comment-list left-padding">
                  <div className="single-comment justify-content-between d-flex">
                    <div className="user justify-content-between d-flex">
                      <div className="thumb">
                        <img src={c3} alt="" />
                      </div>
                      <div className="desc">
                        <h5><a href="#">Annie Stephens</a></h5>
                        <p className="date">December 4, 2017 at 3:12 pm </p>
                        <p className="comment">
                          Never say goodbye till the end comes!
                        </p>
                      </div>
                    </div>
                    <div className="reply-btn">
                      <a className="btn-reply">reply</a>
                    </div>
                  </div>
                </div>
                <div className="comment-list">
                  <div className="single-comment justify-content-between d-flex">
                    <div className="user justify-content-between d-flex">
                      <div className="thumb">
                        <img src={c4} alt="" />
                      </div>
                      <div className="desc">
                        <h5><a href="#">Maria Luna</a></h5>
                        <p className="date">December 4, 2017 at 3:12 pm </p>
                        <p className="comment">
                          Never say goodbye till the end comes!
                        </p>
                      </div>
                    </div>
                    <div className="reply-btn">
                      <a className="btn-reply">reply</a>
                    </div>
                  </div>
                </div>
                <div className="comment-list">
                  <div className="single-comment justify-content-between d-flex">
                    <div className="user justify-content-between d-flex">
                      <div className="thumb">
                        <img src={c5} alt="" />
                      </div>
                      <div className="desc">
                        <h5><a href="#">Ina Hayes</a></h5>
                        <p className="date">December 4, 2017 at 3:12 pm </p>
                        <p className="comment">
                          Never say goodbye till the end comes!
                        </p>
                      </div>
                    </div>
                    <div className="reply-btn">
                      <a className="btn-reply">reply</a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="comment-form">
                <h4>Leave a Reply</h4>
                <form>
                  <div className="form-group form-inline">
                    <div className="form-group col-lg-6 col-md-12 name">
                      <input type="text" className="form-control" id="name" placeholder="Enter Name"  />
                    </div>
                    <div className="form-group col-lg-6 col-md-12 email">
                      <input type="email" className="form-control" id="email" placeholder="Enter email address"  />
                    </div>
                  </div>
                  <div className="form-group">
                    <input type="text" className="form-control" id="subject" placeholder="Subject"  />
                  </div>
                  <div className="form-group">
                    <textarea className="form-control mb-10" rows={5} name="message" placeholder="Messege" required defaultValue={""} />
                  </div>
                  <a href="#" className="primary-btn fill">Post Comment</a>
                </form>
              </div>
            </div>
            <div className="col-lg-3 col-md-12 col-sm-12">
              <div className="row">
                <div className="col-lg-12 col-md-5 col-sm-6">
                  <div className="single-post mb-20">
                    <div className="thumb">
                      <div className="relative">
                        <img className="f-img img-fluid w-100" src={c1} alt="" />
                        <div className="overlay overlay-bg" />
                      </div>
                    </div>
                    <div className="details">
                      <div className="top-part d-flex justify-content-between">
                        <div>
                          <h4>United States</h4>
                          <p>Saturday, September 08, 2018</p>
                        </div>
                        <div>
                          <span className="lnr lnr-arrow-down text-white" />
                        </div>
                      </div>
                      <div className="middle-part">
                        <h1>28ºC</h1>
                        <p>Partly Cloudy</p>
                      </div>
                      <div className="bottom-part">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 col-md-5 col-sm-6">
                  <div className="single-post">
                    <div className="thumb">
                      <div className="relative">
                        <img className="f-img img-fluid w-100" src={c2} alt="" />
                        <div className="overlay overlay-bg" />
                      </div>
                    </div>
                    <div className="details">
                      <div className="bottom d-flex justify-content-between">
                        <div>
                          <h4 className="mt-0">Recent Playlist</h4>
                          <p>Selected by techmania</p>
                        </div>
                        <div>
                          <span className="lnr lnr-arrow-down text-white" />
                        </div>
                      </div>
                      <ul className="list">
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
};

export default PostDetails;