import React from 'react';

import t1 from '../../assets/img/tech-news/t1.jpg'
import t2 from '../../assets/img/tech-news/t2.jpg'
import t3 from '../../assets/img/tech-news/t3.jpg'
import t4 from '../../assets/img/tech-news/t4.jpg'
import p1 from '../../assets/img/popular-news/p1.jpg'
import p2 from '../../assets/img/popular-news/p2.jpg'
import p3 from '../../assets/img/popular-news/p3.jpg'
import p4 from '../../assets/img/popular-news/p4.jpg'
import i3 from '../../assets/img/int-news/i3.jpg'
import c1 from '../../assets/img/c1.jpg'
import c2 from '../../assets/img/c2.jpg'
const MainArea = () => {
    return (
        <div className="category-area">
        <div className="container">
          <div className="row">
            <div className="col-lg-9 col-md-12">
              {/* Start Technology News Area */}
              <section className>
                <div className="row">
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={t1} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={t2} alt="" />
                          <a className="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM">
                            <img src="img/play-icon.png" className="vdo-btn" alt="" />
                          </a>
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={t3} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={t4} alt="" />
                          <a className="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM">
                            <img src="img/play-icon.png" className="vdo-btn" alt="" />
                          </a>
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={p1} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={p3} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={p2} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={p4} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-5 col-sm-6">
                    <div className="single-post single-int mb-40">
                      <div className="thumb">
                        <div className="relative">
                          <img className="f-img img-fluid mx-auto" src={i3} alt="" />
                        </div>
                      </div>
                      <div className>
                        <div className="bottom mt-10">
                          <div>
                            <a href="#" className="primary-btn">gadgets</a>
                            <a href="#"><span>September 14, 2018</span></a>
                          </div>
                        </div>
                        <a href="#">
                          <h4>Facts Why Inkjet Printing Is
                            Appealing Compared</h4>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="#" className="primary-btn center fill mt-30">read more</a>
              </section>
              {/* End Technology News Area */}
            </div>
            <div className="col-lg-3 col-md-12 col-sm-12">
              <div className="row">
                <div className="col-lg-12 col-md-5 col-sm-6">
                  <div className="single-post mb-20">
                    <div className="thumb">
                      <div className="relative">
                        <img className="f-img img-fluid w-100" src={c1} alt="" />
                        <div className="overlay overlay-bg" />
                      </div>
                    </div>
                    <div className="details">
                      <div className="top-part d-flex justify-content-between">
                        <div>
                          <h4>United States</h4>
                          <p>Saturday, September 08, 2018</p>
                        </div>
                        <div>
                          <span className="lnr lnr-arrow-down text-white" />
                        </div>
                      </div>
                      <div className="middle-part">
                        <h1>28ºC</h1>
                        <p>Partly Cloudy</p>
                      </div>
                      <div className="bottom-part">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 col-md-5 col-sm-6">
                  <div className="single-post">
                    <div className="thumb">
                      <div className="relative">
                        <img className="f-img img-fluid w-100" src={c2} alt="" />
                        <div className="overlay overlay-bg" />
                      </div>
                    </div>
                    <div className="details">
                      <div className="bottom d-flex justify-content-between">
                        <div>
                          <h4 className="mt-0">Recent Playlist</h4>
                          <p>Selected by techmania</p>
                        </div>
                        <div>
                          <span className="lnr lnr-arrow-down text-white" />
                        </div>
                      </div>
                      <ul className="list">
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default MainArea;