import React from 'react';

const TopPostArea = () => {
    return (
        <section className="top-post-area">
            <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12 p-0">
                <div className="hero-nav-area">
                    <h1 className="text-white">News Archive</h1>
                    <p className="text-white link-nav">
                    <a href="index.html">Home </a>
                    <span className="lnr lnr-arrow-right" />
                    <a href="archive.html">archive</a>
                    </p>
                </div>
                </div>
            </div>
            </div>
        </section>
    );
};

export default TopPostArea;