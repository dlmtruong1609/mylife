import React from 'react';
import i1 from '../../assets/img/int-news/i1.jpg'
import i2 from '../../assets/img/int-news/i2.jpg'
import i3 from '../../assets/img/int-news/i3.jpg'
import ad from '../../assets/img/ad-300-600.jpg'
const InternalNews = () => {
    return (
        <section className="int-news-area section-gap-top">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-12">
              <div className="row">
                <div className="col-lg-12">
                  <div className="section-title">
                    <h2 className="heading">International News</h2>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-7 col-md-8">
                  <div className="single-post single-int mb-40">
                    <div className="thumb">
                      <div className="relative">
                        <img className="f-img w-100 img-fluid mx-auto" src={i1} alt="" />
                      </div>
                    </div>
                    <div>
                      <div className="bottom pt-30 d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                          <a href="#" className="primary-btn">gadgets</a>
                          <a href="#"><span>September 14, 2018</span></a>
                        </div>
                        <div className="meta">
                          <span className="lnr lnr-bubble" /> 04
                        </div>
                      </div>
                      <a href="#">
                        <h4>Dealing With Technical Support with Printing Is Very
                          Appealing Comp 10 Useful Tips</h4>
                      </a>
                      <p>
                        It won’t be a bigger problem to find one video game lover in your neighbor. Since the introduction of Virtual
                        Game, it has beenachieving great heights so far as its popularity and technological advancement are
                        concerned.
                        The history of video game is as interesting as a fairy tale. The quality of today’s video game was not at all
                        there when video game first conceptualized and played ever
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-5 col-md-12">
                  <div className="row">
                    <div className="col-lg-12 col-md-6 col-sm-6">
                      <div className="single-post single-int mb-40">
                        <div className="thumb">
                          <div className="relative">
                            <img className="f-img w-100 img-fluid mx-auto" src={i2} alt="" />
                          </div>
                        </div>
                        <div>
                          <div className="bottom mt-10">
                            <div>
                              <a href="#" className="primary-btn">gadgets</a>
                              <a href="#"><span>September 14, 2018</span></a>
                            </div>
                          </div>
                          <a href="#">
                            <h4 className="mt-15">Facts Why Inkjet Printing Is Very
                              Appealing Compared</h4>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-6 col-sm-6">
                      <div className="single-post single-int mb-40">
                        <div className="thumb">
                          <div className="relative">
                            <img className="f-img w-100 img-fluid mx-auto" src={i3} alt="" />
                          </div>
                        </div>
                        <div>
                          <div className="bottom mt-10">
                            <div>
                              <a href="#" className="primary-btn">gadgets</a>
                              <a href="#"><span>September 14, 2018</span></a>
                            </div>
                          </div>
                          <a href="#">
                            <h4>Facts Why Inkjet Printing Is Very
                              Appealing Compared</h4>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="ad-widget-wrap">
                <img src={ad} alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
};

export default InternalNews;