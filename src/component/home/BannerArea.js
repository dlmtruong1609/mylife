import React from 'react';

import ReactOwlCarousel from 'react-owl-carousel';

import lb1 from '../../assets/img/banner/lb1.jpg'
import lb2 from '../../assets/img/banner/lb2.jpg'
import lb3 from '../../assets/img/banner/lb3.jpg'
import bc1 from '../../assets/img/banner/bc1.jpg'
import br1 from '../../assets/img/banner/br1.jpg'
import br2 from '../../assets/img/banner/br2.jpg'
const BannerArea = () => {
    const options = {
        items: 1,
        loop: true,
        margin: 0,
        autoplayHoverPause: true,
        dots: false,
        autoplay: 2000,
        nav: true,
        navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"]
    }
    let isHomePage = ( ) => {
        let pathName = window.location.pathname;
        if (pathName === '/' || pathName === '/home') {
          return true;
        }
        return false;
    }
    return isHomePage() ? (
        <section className="banner-area">
        <div className="container-fluid">
        <div className="row">
            <div className="col-lg-3 col-md-12">
            <div className="row">
                <div className="col-lg-12 col-md-4 col-sm-6">
                <div className="single-post mb-03">
                    <div className="thumb">
                    <div className="relative">
                        <img className="f-img img-fluid mx-auto" src={lb1} alt="" />
                        <div className="overlay overlay-bg" />
                    </div>
                    </div>
                    <div className="details">
                    <div className="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                       <a className="primary-btn" href="">Du lịch</a>
                        <a href="#"><span>12/09/2019</span></a>
                        </div>
                        <div className="meta">
                        <span className="lnr lnr-bubble" /> 12h00
                        </div>
                    </div>
                    <a href="#">
                        <h4>2nd Gen Smoke CO Alarm</h4>
                    </a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        Consectetur adipisicing elit sed do eiusmod tempor.
                    </p>
                    </div>
                </div>
                </div>
                <div className="col-lg-12 col-md-4 col-sm-6">
                <div className="single-post mb-03">
                    <div className="thumb">
                    <div className="relative">
                        <img className="f-img img-fluid mx-auto" src={lb2} alt="" />
                        <div className="overlay overlay-bg" />
                    </div>
                    </div>
                    <div className="details">
                    <div className="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                       <a className="primary-btn" href="">Du lịch</a>
                        <a href="#"><span>12/09/2019</span></a>
                        </div>
                        <div className="meta">
                        <span className="lnr lnr-bubble" /> 12h00
                        </div>
                    </div>
                    <a href="#">
                        <h4>2nd Gen Smoke CO Alarm</h4>
                    </a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        Consectetur adipisicing elit sed do eiusmod tempor.
                    </p>
                    </div>
                </div>
                </div>
                <div className="col-lg-12 col-md-4 col-sm-6">
                <div className="single-post">
                    <div className="thumb">
                    <div className="relative">
                        <img className="f-img img-fluid mx-auto" src={lb3} alt="" />
                        <div className="overlay overlay-bg" />
                    </div>
                    </div>
                    <div className="details">
                    <div className="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                       <a className="primary-btn" href="">Du lịch</a>
                        <a href="#"><span>12/09/2019</span></a>
                        </div>
                        <div className="meta">
                        <span className="lnr lnr-bubble" /> 12h00
                        </div>
                    </div>
                    <a href="#">
                        <h4>2nd Gen Smoke CO Alarm</h4>
                    </a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        Consectetur adipisicing elit sed do eiusmod tempor.
                    </p>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <ReactOwlCarousel {...options} className="col-lg-6 col-md-8 owl-carousel active-banner owl-theme owl-loaded">
            <div className="single-post">
                <div className="thumb">
                <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={bc1} alt="" />
                    <div className="overlay overlay-bg" />
                </div>
                </div>
                <div className="details">
                    <div className="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                       <a className="primary-btn" href="">Du lịch</a>
                        <a href="#"><span>12/09/2019</span></a>
                        </div>
                        <div className="meta">
                        <span className="lnr lnr-bubble" /> 12h00
                        </div>
                    </div>
                <a href="#">
                    <h4 className="lg-font">Nest Protect: 2nd Gen Smoke <br />
                    + CO Alarm</h4>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. Lorem
                    ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                </p>
                <a href="#" className="primary-btn fill mt-30">read more</a>
                </div>
            </div>
            <div className="single-post">
                <div className="thumb">
                <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={bc1} alt="" />
                    <div className="overlay overlay-bg" />
                </div>
                </div>
                <div className="details">
                <div className="bottom d-flex justify-content-start align-items-center flex-wrap">
                    <div>
                    <a href="#" className="primary-btn">gadgets</a>
                    <a href="#"><span>September 14, 2018</span></a>
                    </div>
                    <div className="meta">
                    <span className="lnr lnr-bubble" /> 04
                    </div>
                </div>
                <a href="#">
                    <h4 className="lg-font">Nest Protect: 2nd Gen Smoke <br />
                    + CO Alarm</h4>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. Lorem
                    ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                </p>
                <a href="#" className="primary-btn fill mt-30">read more</a>
                </div>
            </div>
            </ReactOwlCarousel>
            <div className="col-lg-3 col-md-4">
            <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-6">
                <div className="single-post mb-03">
                    <div className="thumb">
                    <div className="relative">
                        <img className="f-img img-fluid mx-auto" src={br2} alt="" />
                        <div className="overlay overlay-bg" />
                    </div>
                    </div>
                    <div className="details">
                    <div className="top-part d-flex justify-content-between">
                        <div>
                        <h4>United States</h4>
                        <p>Saturday, September 08, 2018</p>
                        </div>
                        <div>
                        <span className="lnr lnr-arrow-down text-white" />
                        </div>
                    </div>
                    <div className="middle-part">
                        <h1>28ºC</h1>
                        <p>Partly Cloudy</p>
                    </div>
                    <div className="bottom-part">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    </div>
                    </div>
                </div>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-6">
                <div className="single-post">
                    <div className="thumb">
                    <div className="relative">
                        <img className="f-img img-fluid mx-auto" src={br1} alt="" />
                        <div className="overlay overlay-bg" />
                    </div>
                    </div>
                    <div className="details">
                    <div className="bottom d-flex justify-content-between">
                        <div>
                        <h4 className="mt-0">Recent Music Playlist</h4>
                        <p>Selected by techmania</p>
                        </div>
                        <div>
                        <span className="lnr lnr-arrow-down text-white" />
                        </div>
                    </div>
                    <ul className="list">
                        <li>
                        <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        </a>
                        </li>
                        <li>
                        <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        </a>
                        </li>
                        <li>
                        <a href="#">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                        </a>
                        </li>
                    </ul>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        </section>
    ): null;
    
};

export default BannerArea;