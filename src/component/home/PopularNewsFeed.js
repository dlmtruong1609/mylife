import React from 'react';

import p1 from '../../assets/img/popular-news/p1.jpg'
import p2 from '../../assets/img/popular-news/p2.jpg'
import p3 from '../../assets/img/popular-news/p3.jpg'
import p4 from '../../assets/img/popular-news/p4.jpg'
import pvdo from '../../assets/img/popular-news/p-vdo.jpg'
const PopularNewsFeed = () => {
  return (
    <section className="popular-news-area">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="section-title">
            <h2 className="heading">Popular News Feed</h2>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-3 col-md-6 col-sm-6">
          <div className="single-post single-int mb-40">
            <div className="thumb">
              <div className="relative">
                <img className="f-img img-fluid mx-auto" src={p1} alt="" />
              </div>
            </div>
            <div>
              <div className="bottom mt-10 d-flex justify-content-between align-items-center flex-wrap">
                <div>
                  <a href="#" className="primary-btn">gadgets</a>
                  <a href="#"><span>September 14, 2018</span></a>
                </div>
              </div>
              <a href="#">
                <h4>Facts Why Inkjet Printing Is Very
                  Appealing Compared</h4>
              </a>
            </div>
          </div>
          <div className="single-post single-int mb-40">
            <div className="thumb">
              <div className="relative">
                <img className="f-img img-fluid mx-auto" src={p2} alt="" />
              </div>
            </div>
            <div>
              <div className="bottom mt-10 d-flex justify-content-between align-items-center flex-wrap">
                <div>
                  <a href="#" className="primary-btn">gadgets</a>
                  <a href="#"><span>September 14, 2018</span></a>
                </div>
              </div>
              <a href="#">
                <h4>Facts Why Inkjet Printing Is Very
                  Appealing Compared</h4>
              </a>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-6">
          <div className="single-post single-int mb-40">
            <div className="thumb">
              <div className="relative">
                <img className="f-img img-fluid mx-auto" src={p3} alt="" />
              </div>
            </div>
            <div>
              <div className="bottom mt-10">
                <div>
                  <a href="#" className="primary-btn">gadgets</a>
                  <a href="#"><span>September 14, 2018</span></a>
                </div>
              </div>
              <a href="#">
                <h4 className="mt-15">Facts Why Inkjet Printing Is Very
                  Appealing Compared</h4>
              </a>
            </div>
          </div>
          <div className="single-post single-int mb-40">
            <div className="thumb">
              <div className="relative">
                <img className="f-img img-fluid mx-auto" src={p4} alt="" />
              </div>
            </div>
            <div>
              <div className="bottom mt-10">
                <div>
                  <a href="#" className="primary-btn">gadgets</a>
                  <a href="#"><span>September 14, 2018</span></a>
                </div>
              </div>
              <a href="#">
                <h4>Facts Why Inkjet Printing Is Very
                  Appealing Compared</h4>
              </a>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-6 col-sm-12">
          <div className="single-post single-int mb-40">
            <div className="thumb">
              <div className="relative">
                <img className="f-img img-fluid mx-auto" src={pvdo} alt="" />
                <a className="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM">
                  <img src="img/play-icon.png" className="vdo-btn" alt="" />
                </a>
              </div>
            </div>
            <div>
              <div className="bottom mt-10 d-flex justify-content-between align-items-center flex-wrap">
                <div>
                  <a href="#" className="primary-btn">gadgets</a>
                  <a href="#"><span>September 14, 2018</span></a>
                </div>
                <div className="meta">
                  <span className="lnr lnr-bubble" /> 04
                </div>
              </div>
              <a href="#">
                <h4 className="mt-15">Dealing With Technical Support with Printing Is Very
                  Appealing Comp 10 Useful Tips</h4>
              </a>
              <p>
                It won’t be a bigger problem to find one video game lover in your neighbor. Since the introduction of Virtual
                Game, it has beenachieving great heights so far as its popularity and technological advancement are
                concerned.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  );
};

export default PopularNewsFeed;