import React from 'react';


import e1 from '../../assets/img/editors-pick/e1.jpg'
import e2 from '../../assets/img/editors-pick/e2.jpg'
import e3 from '../../assets/img/editors-pick/e3.jpg'
import e4 from '../../assets/img/editors-pick/e4.jpg'
const EditorPickArea = () => {
    return (
        <section className="editors-area">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-6">
              <div className="single-post d-flex single-post2 mb-40">
                <div className="thumb">
                  <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={e1} alt="" />
                  </div>
                </div>
                <div>
                  <div className="bottom">
                    <div>
                      <a href="#" className="primary-btn">gadgets</a>
                      <a href="#"><span>September 14, 2018</span></a>
                    </div>
                  </div>
                  <a href="#">
                    <h4>2nd Gen Smoke CO Alarm time <br />
                      to get up from sleep</h4>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6">
              <div className="single-post d-flex single-post2 mb-40">
                <div className="thumb">
                  <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={e2} alt="" />
                  </div>
                </div>
                <div>
                  <div className="bottom">
                    <div>
                      <a href="#" className="primary-btn">gadgets</a>
                      <a href="#"><span>September 14, 2018</span></a>
                    </div>
                  </div>
                  <a href="#">
                    <h4>2nd Gen Smoke CO Alarm time <br />
                      to get up from sleep</h4>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6">
              <div className="single-post d-flex single-post2">
                <div className="thumb">
                  <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={e3} alt="" />
                  </div>
                </div>
                <div>
                  <div className="bottom">
                    <div>
                      <a href="#" className="primary-btn">gadgets</a>
                      <a href="#"><span>September 14, 2018</span></a>
                    </div>
                  </div>
                  <a href="#">
                    <h4>2nd Gen Smoke CO Alarm time <br />
                      to get up from sleep</h4>
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6">
              <div className="single-post d-flex single-post2">
                <div className="thumb">
                  <div className="relative">
                    <img className="f-img img-fluid mx-auto" src={e4} alt="" />
                  </div>
                </div>
                <div>
                  <div className="bottom">
                    <div>
                      <a href="#" className="primary-btn">gadgets</a>
                      <a href="#"><span>September 14, 2018</span></a>
                    </div>
                  </div>
                  <a href="#">
                    <h4>2nd Gen Smoke CO Alarm time <br />
                      to get up from sleep</h4>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
};

export default EditorPickArea;