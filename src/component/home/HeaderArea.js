import React, { useRef, useEffect } from 'react';

import { Link } from 'react-router-dom';
import logo from '../../assets/img/logo.png'
const HeaderArea = () => {
  const navbar = useRef(null);
  let sticky = 0;
  let getOffsetTopNavbar = () => {
    let temp = navbar.current.offsetTop;
    if(temp > 0) sticky = temp;
  }
  let stickFunction= () => {
     if (window.pageYOffset >= sticky) {
        navbar.current.classList.add("sticky");
      } else {
        navbar.current.classList.remove("sticky");
      }
  };
  useEffect(() => {
      window.addEventListener('load', getOffsetTopNavbar, true);
      window.addEventListener('scroll', stickFunction, true);
  });
  useEffect(() => {
    return () => {
      window.removeEventListener('scroll', getOffsetTopNavbar);
      window.removeEventListener('scroll', stickFunction);
    }
  })
    return (
        <header>
        <div ref={navbar} className="main-menu" id="main-menu">
          <div className="container-fluid">
            <div className="row align-items-center justify-content-between">
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div>
              <Link className="navbar-brand" to="/">
                <img src={logo} alt="" />
              </Link>
            </div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse nav-mr" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="#">Trang chủ <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Du lịch</a>
                </li>
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ẩm thực
                  </a>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a className="dropdown-item" href="#">Thời trang</a>
                    <a className="dropdown-item" href="#">Thiết kế</a>
                    <div className="dropdown-divider" />
                    <a className="dropdown-item" href="#">Lễ hội</a>
                  </div>
                </li>
                <li className="nav-item">
                  <a className="nav-link disabled" href="#">Văn hoá</a>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0 pl-5">
                <input className="form-control mr-sm-2" type="search" placeholder="Du lịch, ẩm thực..." aria-label="Search" />
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm kiếm</button>
              </form>
            </div>
          </nav>
            </div>
          </div>
        </div>
      </header>
    );
};

export default HeaderArea;