import React from 'react';

import n1 from '../../assets/img/n1.jpg'
import n2 from '../../assets/img/n2.jpg'
import logo from '../../assets/img/logo.png'
const FooterArea = () => {
    return (
        <footer className="footer-area section-gap-top">
        <div className="container">
          <div className="row pb-10">
            <div className="col-lg-4 col-md-6">
              <div className="single-footer-widget">
                <div className="mb-40">
                  <img src={logo} alt="" />
                </div>
                <p>
                  Technology and gadgets Adapter (MPA) is our favorite iPhone solution, since it lets you use the headphones
                  you’re most comfortable with. It has an iPhone-compatible jack at one end and a microphone module with an
                  Answer/End/Pause button and a female 3.5mm audio jack for connectingheadphones
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-6">
              <div className="single-footer-widget">
                <h6 className="heading">Quick Links</h6>
                <div className="row">
                  <ul className="col footer-nav">
                    <li><a href="#">Sitemaps</a></li>
                    <li><a href="#">Categories</a></li>
                    <li><a href="#">Archives</a></li>
                    <li><a href="#">Advertise</a></li>
                    <li><a href="#">Ad Choice</a></li>
                  </ul>
                  <ul className="col footer-nav">
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Help Center</a></li>
                    <li><a href="#">Newsletters</a></li>
                    <li><a href="#">Feedback</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-12">
              <div className="single-footer-widget mail-chimp">
                <h6 className="heading">Most Viewed News</h6>
                <div className="footer-news">
                  <div className="single-news d-flex">
                    <div className="image">
                      <img src={n1} alt="" />
                    </div>
                    <div className="details">
                      <a href="#">
                        <h4>Converter Ipod Video Taking Portable
                          Video Viewing To A Whole Level</h4>
                      </a>
                      <div className="d-flex justify-content-between">
                        <p>March 14, 2018</p>
                        <p className="meta">
                          <span className="lnr lnr-bubble" />
                          <span className="likes">04</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="single-news d-flex">
                    <div className="image">
                      <img src={n2} alt="" />
                    </div>
                    <div className="details">
                      <a href="#">
                        <h4>Converter Ipod Video Taking Portable
                          Video Viewing To A Whole Level</h4>
                      </a>
                      <div className="d-flex justify-content-between">
                        <p>March 14, 2018</p>
                        <p className="meta">
                          <span className="lnr lnr-bubble" />
                          <span className="likes">04</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="copyright-text">
          <div className="container">
            <div className="row footer-bottom d-flex justify-content-between">
              <p className="col-lg-8 col-sm-6 footer-text m-0 text-white">{/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
                Copyright © All rights reserved | This template is made with <i className="fa fa-heart-o" aria-hidden="true" /> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}</p>
              <div className="col-lg-4 col-sm-6 footer-social">
                <a href="#"><i className="fa fa-facebook" /></a>
                <a href="#"><i className="fa fa-twitter" /></a>
                <a href="#"><i className="fa fa-youtube-play" /></a>
                <a href="#"><i className="fa fa-pinterest" /></a>
                <a href="#"><i className="fa fa-rss" /></a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
};

export default FooterArea;