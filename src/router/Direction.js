import React from 'react';

import { Router, Switch, Route, BrowserRouter } from 'react-router-dom';

import HomePage from '../page/HomePage';
import Travel from '../page/Travel';
import InternalNews from '../component/home/InternalNews';
import PostDetails from '../component/pages/PostDetails';
import TopPostArea from '../component/pages/TopPostArea';

const Direction = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/dulich">
                <Travel />
                </Route>
                <Route path="/post/:id">
                <PostDetails />
                </Route>
                <Route path="/">
                <HomePage />
                </Route>
            </Switch>
        </BrowserRouter>
    );
};

export default Direction;